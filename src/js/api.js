// Posts
const url = "/src/posts";
const json = "/src/posts/posts.json";

// Feed type
var feed = "grid";

// Sorting algorithm thingy
// https://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value
function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        /* next line works with strings and numbers, 
         * and you may want to customize it to your needs
         */
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

// Gets posts
function getPosts() {

    // Removes any feeds
    $("div.feed").empty();
    let posts = [];

    // Pareses JSON data
    $.getJSON(json, function(data) {

        // Gets length
        const postCount = Object.keys(data).length;
        let postCountDown = postCount;

        // Appends images to array
        for (i = 0; i < postCount; i++) {

            // Date format: YYYYmmDDhhMM
            let currentId = data[postCountDown]["id"];
            let currentDate = data[postCountDown]["date"];
            let currentSrc = data[postCountDown]["src"];
            let currentAuthor = data[postCountDown]["author"];

            // Adds images
            posts.push({"id": currentId,"date": currentDate, "src": currentSrc, "author": currentAuthor});

            // Goes down for some reason
            postCountDown--;
        }

        // Sorts into newest-first
        posts.sort(dynamicSort("date")).reverse();
        console.log(posts);

        // Add correct feed type
        $("div.feed").append("<div class='" + feed + "' id='grid'></div>")

        // Grid layout
        if (feed == "grid") {

            // Loops through each post
            for(i = 0; i < postCount; i++) {

                // Adds post
                $("div.grid").append("<img class='post' id='" + posts[i].id + "' src='" + url + "/" + posts[i].src + "'>");
            }
        
            // Scroll layout
        } else if (feed == "scroll") {

            // Loops through each post
            for (i = 0; i < postCount; i++) {
                let currentPost = "div.post#" + posts[i].id;

                // Adds wrapper
                $("div.scroll").append("<div class='post' id='" + posts[i].id + "'></div>")

                // Adds author
                $(currentPost).append("<p class='author'>" + posts[i].author + "</p>")

                // Adds image
                $(currentPost).append("<img class='post' id='" + currentPost + "' src='" + url + "/" + [posts[i].src] + "'>");
            }
        }
    });
};

getPosts();

$("a#layout").click(function() {
    if (feed == "grid") {
        feed = "scroll";
    } else if (feed == "scroll") {
        feed = "grid";
    };

    getPosts();
    $("a#layout").html(feed[0].toUpperCase() + feed.substring(1))
});