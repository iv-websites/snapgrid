// Posts Folder
var folder = "src/posts/";
var posts = 8;
var post = posts;
var scrollLocation = null;
var data = null;
var jsonTest = null;


for (i=0;i<posts;i++) {
    // Gets post name
    postDir = folder + post + ".jpg"
    
    // Creates element
    $("div.grid").append("<img class='post' id='" + post + "' src='" + postDir + "'>");

    post -= 1;
}

// View post fullscreen
$("img.post").click(function() {

    // Save scroll location
    scrollLocation = $(window).scrollTop();

    // Show fullscreen post window
    $("div.fullscreen").removeClass("hidden");

    // Gets post
    currentPost = $(this).attr("id");

    // Get post and display it
    $("img#fullpost").attr("src", "/src/posts/" + currentPost + ".jpg");

    $.getJSON("src/posts/posts.json", function(data) {
        jsonTest = data;
        $("p#author").html(data[currentPost]["author"]);
        $("p#date").html(data[currentPost]["date"]);
        $("p#desc").html(data[currentPost]["desc"]);
        $("p#iso").html("ISO: " + data[currentPost]["iso"]);
        $("p#fstop").html("Aperture: f/" + data[currentPost]["aperture"]);

        console.log(Object.keys(data).length);
    });

    console.log(jsonTest);

    // Disable scrolling
    $("html, body").css({
        overflow: 'hidden',
        height: '100%'
    });
});


// Close fullscreen viewer
$("div.fullscreen").click(function() {

    // Hides it
    $(this).addClass("hidden");

    // Enables scrolling
    $("html, body").css({
        overflow: 'initial',
        height: 'auto'
    });

    // Restore scroll position
    $(window).scrollTop(scrollLocation);
})